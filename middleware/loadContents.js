export default async function ({ store }) {
    await store.dispatch('loadResidential', {})
    await store.dispatch('loadIndustrial', {})
    await store.dispatch('loadUseCases', {})
    await store.dispatch('loadBenefits', {})
    await store.dispatch('loadSettings', {})
    await store.dispatch('loadAppDetails', {})
    await store.dispatch('loadCategories', {})
}