let mutations = {
    GET_RESIDENTIAL(state, residential) { state.residential = residential },
    GET_INDUSTRIAL(state, industrial) { state.industrial = industrial },
    GET_USE_CASES(state, useCases) { state.useCases = useCases },
    GET_BENEFITS(state, benefits) { state.benefits = benefits },
    GET_SETTINGS(state, settings) { state.settings = settings },
    GET_APP_DETAILS(state, appDetails) { state.appDetails = appDetails },
    GET_CATEGORIES(state, categories) { state.categories = categories }
}

export default mutations