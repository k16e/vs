let state = () => ({
    residential: [],
    industrial: [],
    useCases: [],
    benefits: [],
    settings: {},
    appDetails: {},
    categories: []
})

export default state