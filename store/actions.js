let actions = {
    loadResidential({ commit }) {
        try {
            return this.$storyapi.get(`cdn/stories/`, {
                starts_with: 'solutions/',
                resolve_relations: 'category',
                'filter_query[category][in]': '1b66bdec-f21f-47fa-8331-05b35fef00b5',
                sort_by: 'content.position:asc'
            })
                .then(res => { commit('GET_RESIDENTIAL', res.data.stories) })
        } catch (error) { console.log(error) }
    },
    loadIndustrial({ commit }) {
        try {
            return this.$storyapi.get(`cdn/stories/`, {
                starts_with: 'solutions/',
                resolve_relations: 'category',
                'filter_query[category][in]': 'e99a95d8-fa1e-4ac1-b13f-c5adff05eb44',
                sort_by: 'content.position:asc'
            })
                .then(res => { commit('GET_INDUSTRIAL', res.data.stories) })
        } catch (error) { console.log(error) }
    },
    loadUseCases({ commit }) {
        try {
            return this.$storyapi.get(`cdn/stories/`, { starts_with: 'use-cases/' })
                .then(res => { commit('GET_USE_CASES', res.data.stories) })
        } catch (error) { console.log(error) }
    },
    loadBenefits({ commit }) {
        try {
            return this.$storyapi.get(`cdn/stories/`, { starts_with: 'benefits/' })
                .then(res => { commit('GET_BENEFITS', res.data.stories) })
        } catch (error) { console.log(error) }
    },
    loadSettings({ commit }) {
        try {
            return this.$storyapi.get(`cdn/stories/settings`, { resolve_links: 'url' })
                .then((res) => { commit('GET_SETTINGS', res.data.story.content) })
        } catch (error) { console.log(error) }
    },
    loadAppDetails({ commit }) {
        try {
            return this.$storyapi.get(`cdn/stories/pages/app`, {})
                .then((res) => { commit('GET_APP_DETAILS', res.data.story.content) })
        } catch (error) { console.log(error) }
    },
    loadCategories({ commit }) {
        try {
            return this.$storyapi.get(`cdn/stories/`, {
                starts_with: 'categories/',
                sort_by: 'content.position:asc'
            })
                .then((res) => { commit('GET_CATEGORIES', res.data.stories) })
        } catch (error) { console.log(error) }
    }
}

export default actions