let defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
    theme: {
        container: { center: true },
        extend: {
            colors: {
                'accent': {
                    '50': '#5272cc',
                    '100': '#4868c2',
                    '200': '#3e5eb8',
                    '300': '#3454ae',
                    '400': '#2a4aa4',
                    '500': '#20409a',
                    '600': '#163690',
                    '700': '#0c2c86',
                    '800': '#02227c',
                    '900': '#001872'
                }
            }
        },
        screens: {
            'xs': '475px',
            ...defaultTheme.screens,
        }
    },
    plugins: [
        require('@tailwindcss/typography'),
        require('@tailwindcss/aspect-ratio')
    ]
}