import { fetchSitemapRoutes } from './utils/sitemap'
export default async () => {
    let routes = await fetchSitemapRoutes()
    return {
        target: 'static',
        telemetry: true,
        generate: { fallback: '404.html' },
        components: true,
        router: { middleware: ['loadContents'] },
        publicRuntimeConfig: {
            baseURL: process.env.BASE_URL,
            legalName: process.env.LEGAL_NAME,
            siteTitle: process.env.SITE_TITLE,
            siteDescription: process.env.SITE_DESCRIPTION,
            contentKey: process.env.CONTENT_KEY
        },
        privateRuntimeConfig: {},
        head: {
            title: process.env.SITE_TITLE,
            meta: [
                { charset: 'utf-8' },
                { name: 'viewport', content: 'width=device-width, initial-scale=1' },
                { hid: 'description', name: 'description', content: process.env.SITE_DESCRIPTION }
            ],
            link: [
                { rel: 'icon', type: 'image/x-icon', href: '/icon.png' },
                { rel: 'stylesheet', href: '/css/flickity.min.css' },
                { hid: 'canonical', rel: 'canonical', href: process.env.BASE_URL || 'http://localhost:3000' }
            ]
        },
        loading: {
            color: '#20409A',
            continuous: true,
        },
        buildModules: [
            '@nuxtjs/tailwindcss',
            '@nuxtjs/svg',
            '@nuxtjs/snipcart'
        ],
        tailwindcss: { jit: true },
        snipcart: { key: 'MmJiZmJhYzEtZjdjZi00ZTVkLTkzNGMtZjJiMTRjOWU3YzJlNjM3NTIyNzU3OTM1MjU0OTE1' },
        modules: [
            '@nuxtjs/pwa',
            ['@nuxtjs/markdownit', {
                html: true,
                injected: true,
                use: ['markdown-it-prism']
            }],
            ['storyblok-nuxt', {
                accessToken: process.env.CONTENT_KEY,
                cacheProvider: 'memory'
            }],
            '@nuxtjs/sitemap'
        ],
        plugins: [
            '~/plugins/vue-scroll-reveal.client',
            '~/plugins/vue-flickity.client',
            '~/plugins/toggleable.client',
            '~/plugins/headroom.client',
            '~/plugins/stripetype.client',
            '~/plugins/back-to-top.client',
            '~/plugins/snipcart.client'
        ],
        pwa: {
            icon: { fileName: 'icon.png' },
            meta: {
                name: process.env.SITE_TITLE,
                author: 'Kabolobari Benakole',
                description: process.env.SITE_DESCRIPTION
            },
            manifest: {
                name: process.env.SITE_TITLE,
                short_name: process.env.SITE_TITLE
            }
        },
        sitemap: {
            hostname: process.env.BASE_URL,
            routes,
        }
    }
}