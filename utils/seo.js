export default (data) => [
    { hid: 'description', name: 'description', content: data.snippet },

    { hid: 'og:site_name', property: 'og:site_name', content: process.env.SITE_TITLE },
    { hid: 'og:type', property: 'og:type', content: 'website' },
    { hid: 'og:url', property: 'og:url', content: data.path },
    { hid: 'og:title', property: 'og:title', content: `${data.title} | ${process.env.SITE_TITLE}` },
    { hid: 'og:description', property: 'og:description', content: data.snippet },
    { hid: 'og:image', property: 'og:image', content: data.image },

    { hid: 'twitter:card', name: 'twitter:card', content: 'summary_large_image' },
    { hid: 'twitter:url', name: 'twitter:url', content: `${data.path}` },
    { hid: 'twitter:title', name: 'twitter:title', content: `${data.title} | ${process.env.SITE_TITLE}` },
    { hid: 'twitter:description', name: 'twitter:description', content: data.snippet },
    { hid: 'twitter:image', name: 'twitter:image', content: data.image }
]