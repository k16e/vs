import StoryblokClient from 'storyblok-js-client'
export let fetchSitemapRoutes = async () => {
    let
        routes = [],
        client = new StoryblokClient({ accessToken: process.env.CONTENT_KEY }),
        { data: legalData } = await client.get('cdn/links', { starts_with: 'legal/' }),
        { data: allPages } = await client.get('cdn/links', { starts_with: 'pages/' }),
        { data: solutionsData } = await client.get('cdn/links', { starts_with: 'solutions/' }),
        { data: useCases } = await client.get('cdn/links', { starts_with: 'use-cases/' })

    routes.push('/')
    Object.values(allPages.links).forEach(item => routes.push(`/${item.slug.substr(6)}`))
    Object.values(solutionsData.links).forEach(item => routes.push(`/${item.slug}`))
    Object.values(useCases.links).forEach(item => routes.push(`/${item.slug}`))
    Object.values(legalData.links).forEach(item => routes.push(`/${item.slug}`))

    return routes
}