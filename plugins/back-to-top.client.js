export default ({ app }, inject) => {
    let backToTop = () => {
        if (document.querySelector('.back-to-top')) {
            let target = document.querySelectorAll('.back-to-top')
            target.forEach(el => {
                el.addEventListener('click', () => {
                    if (window.location.pathname === '/' && window.scrollY !== 0) window.scrollTo(0, 0)
                }, false)
            });
        }
    }
    inject('backToTop', backToTop)
}