export default ({ app }, inject) => {
    let toggleable = () => {
        let
            collapseArr = (arr) => {
                arr.forEach(el => {
                    el.style.height = '0px'
                    el.style.opacity = '0'
                    el.style.overflow = 'hidden'
                    el.style.visibility = 'hidden'
                })
            },
            targets = Array.from(document.querySelectorAll('.js-toggle')),
            toggleableArr = Array.from(document.querySelectorAll('.js-toggleable'))

        collapseArr(toggleableArr)

        targets.forEach(target => {
            target.addEventListener('click', e => {
                let
                    targetDataAttr = e.target.getAttribute('data-toggleable'),
                    toggleableSingle = toggleableArr.filter(el => el.id === targetDataAttr),
                    toggleableEl = toggleableSingle[0]

                e.target.classList.toggle('is-toggled')
                if (e.target.classList.contains('is-toggled')) {
                    toggleableEl.style.height = `${toggleableEl.scrollHeight}px`
                    toggleableEl.style.opacity = '1'
                    toggleableEl.style.overflow = 'auto'
                    toggleableEl.style.visibility = 'visible'
                } else collapseArr(toggleableSingle)
            })
        })
    }
    inject('toggleable', toggleable)
}