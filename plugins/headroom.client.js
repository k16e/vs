export default ({ app }, inject) => {
    let headroom = () => {
        if (document.querySelector('.r-hero')) {
            let
                target = document.querySelector('.r-hero'),
                targetHeight = Math.floor(target.getBoundingClientRect().height) * 0.5,
                topbar = document.querySelector('.js-header'),
                prevYPos = 0,
                toggle = topbar.querySelector('.js-toggle'),
                toggleable = topbar.querySelector('.js-toggleable'),
                body = document.querySelector('body'),
                dimissNav = () => {
                    body.classList.remove('mobile-nav-is-active')
                    if (window.pageYOffset < targetHeight) topbar.classList.remove('bg-black')
                    if (window.pageYOffset < 56.59) topbar.classList.add('is-supersized')
                },
                collapseEl = el => {
                    el.style.height = '0px'
                    el.style.opacity = '0'
                    el.style.overflow = 'hidden'
                    el.style.visibility = 'hidden'
                }

            window.addEventListener('scroll', () => {
                if (window.pageYOffset > 56.59) topbar.classList.remove('is-supersized')
                else topbar.classList.add('is-supersized')

                if (window.pageYOffset > targetHeight) topbar.classList.add('bg-black')
                else topbar.classList.remove('bg-black')

                let currYPos = window.pageYOffset

                if (
                    currYPos < prevYPos ||
                    currYPos == 0 ||
                    (window.innerHeight + currYPos) >= document.body.offsetHeight
                ) topbar.classList.add('is-in-view')
                else topbar.classList.remove('is-in-view')

                prevYPos = currYPos <= 0 ? 0 : currYPos
            }, false)

            toggle.addEventListener('click', () => {
                body.classList.toggle('mobile-nav-is-active')

                if (toggleable.style.height === '0px' && !topbar.classList.contains('bg-black')) topbar.classList.add('bg-black')
                else if (window.pageYOffset < targetHeight) topbar.classList.remove('bg-black')

                if (topbar.classList.contains('is-supersized')) topbar.classList.remove('is-supersized')
                else if (window.pageYOffset < 56.59) topbar.classList.add('is-supersized')

                window.addEventListener('click', e => {
                    if (!e.target.closest('.js-header') || e.target.matches('a')) {
                        dimissNav()
                        collapseEl(toggleable)
                        toggle.classList.remove('is-toggled')
                    }
                }, false)
            }, false)
        }
    }
    inject('headroom', headroom)
}