export default ({ app }, inject) => {
    let snipcart = () => {
        document.addEventListener('snipcart.ready', () => {
            Snipcart.api.session.setLanguage('en', {
                actions: {
                    continue_shopping: 'Back to Page',
                    back_to_store: 'Back to Site'
                },
                header: {
                    title_cart_summary: 'Cart Summary'
                },
                cart: {
                    empty: `When you add a product to cart, it will show here.`
                }
            })
        })
    }
    inject('snipcart', snipcart)
}