export default ({ app }, inject) => {
    let stripeType = () => {
        let
            triggers = document.querySelectorAll('.r-has-dropdown'),
            backdrop = document.querySelector('.r-nav-backdrop'),
            links = document.querySelectorAll('.r-main-nav a'),
            handleEnter = e => {
                backdrop.classList.add('r-nav-is-shown')
                e.target.classList.add('trigger-enter')
                setTimeout(
                    () => e.target.classList.contains('trigger-enter') && e.target.classList.add('trigger-enter-active'), 150
                )

                let
                    dropdown = e.target.querySelector('.r-dropdown'),
                    caret = e.target.querySelector('.r-flip-on-hover'),
                    coords = dropdown.getBoundingClientRect()

                caret.classList.add('rotate-180')
                backdrop.style.setProperty('height', `${coords.height}px`)
            },
            handleLeave = (e) => {
                let caret = e.target.querySelector('.r-flip-on-hover')

                e.target.classList.remove('trigger-enter', 'trigger-enter-active')
                caret.classList.remove('rotate-180')
                backdrop.classList.remove('r-nav-is-shown')
            }

        triggers.forEach(trigger => trigger.addEventListener('mouseenter', handleEnter, false))
        triggers.forEach(trigger => trigger.addEventListener('mouseleave', handleLeave, false))

        links.forEach(link => link.addEventListener('click', e => {
            let
                li = e.target.closest('li'),
                caret = li.querySelector('.r-flip-on-hover')

            li.classList.remove('trigger-enter', 'trigger-enter-active')
            caret.classList.remove('rotate-180')
            backdrop.classList.remove('r-nav-is-shown')
        }, false))
    }
    inject('stripeType', stripeType)
}