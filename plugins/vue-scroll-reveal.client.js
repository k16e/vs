import Vue from 'vue'
import VueScrollReveal from 'vue-scroll-reveal'

Vue.use(VueScrollReveal, {
    class: 'vs-reveal',
    delay: 0,
    duration: 1000,
    distance: '104px',
    origin: 'bottom'
})